package com.vgrec.xingpublicrepos.utils;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.vgrec.xingpublicrepos.R;

/**
 * author Unknown, created on 02.06.2015.
 */
public class ViewRepoDialog extends DialogFragment {

    private static final String LINKS = "LINKS";
    private DialogItemClickListener itemClickListener;

    public static ViewRepoDialog newInstance(String[] links) {
        Bundle bundle = new Bundle();
        bundle.putStringArray(LINKS, links);

        ViewRepoDialog dialog = new ViewRepoDialog();
        dialog.setArguments(bundle);
        return dialog;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final String[] links = getArguments().getStringArray(LINKS);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.open_link);
        builder.setCancelable(false);
        builder.setItems(links, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                itemClickListener.onItemClicked(links[item]);
                dismiss();
            }
        });
        return builder.create();
    }

    public void setItemClickListener(DialogItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface DialogItemClickListener {
        void onItemClicked(String item);
    }
}
