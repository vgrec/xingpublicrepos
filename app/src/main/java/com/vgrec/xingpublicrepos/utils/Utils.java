package com.vgrec.xingpublicrepos.utils;

import java.io.Closeable;
import java.io.IOException;

/**
 * author Unknown, created on 02.06.2015.
 */
public class Utils {
    public static final String TAG = "XingPublicRepos";

    public static void closeSilently(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
