package com.vgrec.xingpublicrepos.http;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.vgrec.xingpublicrepos.json.JsonParser;
import com.vgrec.xingpublicrepos.model.Repo;
import com.vgrec.xingpublicrepos.utils.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class HttpRequestService extends IntentService {

    private static final String EXTRA_URL = "EXTRA_URL";
    public static final String REPO_RESULTS = "REPO_RESULTS";
    public static final String NEXT_LINK = "NEXT_LINK";
    public static final String LAST_LINK = "LAST_LINK";
    public static final String RESULTS = "RESULTS";

    public HttpRequestService() {
        super("HttpService");
    }

    public static Intent makeIntent(Context context, String url) {
        Intent intent = new Intent(context, HttpRequestService.class);
        intent.putExtra(EXTRA_URL, url);
        return intent;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String url = intent.getStringExtra(EXTRA_URL);
        Result result = executeRequestAndExtractLinks(url);
        sendResult(result);
    }

    private Result executeRequestAndExtractLinks(String url) {
        Result result = new Result();
        try {
            URL address = new URL(url);
            URLConnection connection = address.openConnection();
            InputStream response = (InputStream) connection.getContent();
            result.repos.addAll(new JsonParser().parse(response));

            // Extract next and last links
            Map<String, List<String>> map = connection.getHeaderFields();
            List<String> links = map.get("Link");
            String[] linksArray = links.get(0).split(",");

            result.nextLink = extractLink(linksArray[0]);
            result.lastLink = extractLink(linksArray[1]);
        } catch (IOException e) {
            Log.d(Utils.TAG, e.getMessage(), e);
        }

        return result;
    }

    private String extractLink(String text) {
        int openSym = text.indexOf("<");
        int endSym = text.indexOf(">");
        if (openSym != -1 && endSym != -1) {
            return text.substring(openSym + 1, endSym);
        }
        return null;
    }

    private void sendResult(Result result) {
        Intent intent = new Intent(REPO_RESULTS);
        intent.putParcelableArrayListExtra(RESULTS, result.repos);
        intent.putExtra(NEXT_LINK, result.nextLink);
        intent.putExtra(LAST_LINK, result.lastLink);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /**
     * Holds the list of repos along with the next and last links extracted from headers.
     * <p/>
     * Normally this class would be a top level class, also Parcelable, to be able to send it
     * to the calling activity though Intent. But in the pressure of time, I put it here with default access modifiers on fields.
     */
    private class Result {
        ArrayList<Repo> repos = new ArrayList<>();
        String nextLink;
        String lastLink;
    }

}
