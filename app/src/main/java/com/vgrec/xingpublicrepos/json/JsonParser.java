package com.vgrec.xingpublicrepos.json;

import android.util.Log;

import com.vgrec.xingpublicrepos.model.Repo;
import com.vgrec.xingpublicrepos.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Converts an InputStream to a list of repos.
 * <p/>
 * author Unknown, created on 02.06.2015.
 */
public class JsonParser {

    public List<Repo> parse(InputStream inputStream) {
        List<Repo> repos = new ArrayList<>();

        try {
            JSONArray jsonArray = new JSONArray(inputStreamToString(inputStream));
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject repoObject = (JSONObject) jsonArray.get(i);
                String name = repoObject.getString("name");
                String description = repoObject.getString("description");
                boolean isFork = repoObject.getBoolean("fork");
                String repoUrl = repoObject.getString("html_url");

                JSONObject ownerObject = repoObject.getJSONObject("owner");
                String owner = ownerObject.getString("login");
                String ownerUrl = ownerObject.getString("html_url");

                repos.add(new Repo(name, description, owner, isFork, repoUrl, ownerUrl));
            }

        } catch (JSONException e) {
            Log.d(Utils.TAG, e.getMessage(), e);
        }

        return repos;
    }

    private static String inputStreamToString(InputStream is) {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            reader = new BufferedReader(new InputStreamReader(is));
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            Log.d(Utils.TAG, e.getMessage(), e);
        } finally {
            Utils.closeSilently(reader);
        }

        return sb.toString();
    }

}
