package com.vgrec.xingpublicrepos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.vgrec.xingpublicrepos.R;
import com.vgrec.xingpublicrepos.model.Repo;

import java.util.ArrayList;
import java.util.List;

/**
 * author Unknown, created on 02.06.2015.
 */
public class ReposAdapter extends BaseAdapter {

    private List<Repo> repos = new ArrayList<>();
    private LayoutInflater inflater;
    private LoadMoreListener loadMoreListener;

    public ReposAdapter(Context context, List<Repo> repos) {
        this.repos = repos;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return repos.size();
    }

    @Override
    public Object getItem(int position) {
        return repos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder viewHolder;
        if (view == null) {
            view = inflater.inflate(R.layout.repo_row, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.repoTextView = (TextView) view.findViewById(R.id.repo_text_view);
            viewHolder.ownerTextView = (TextView) view.findViewById(R.id.owner_text_view);
            viewHolder.descriptionTextView = (TextView) view.findViewById(R.id.description_text_view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        Repo repo = repos.get(position);
        viewHolder.repoTextView.setText(repo.getName());

        // Some repositories do not have a description
        // If this is the case, we just hide the description field in the UI
        if (repo.getDescription() != null && repo.getDescription().length() > 0) {
            viewHolder.descriptionTextView.setVisibility(View.VISIBLE);
            viewHolder.descriptionTextView.setText(repo.getDescription());
        } else {
            viewHolder.descriptionTextView.setVisibility(View.GONE);
        }

        viewHolder.ownerTextView.setText(view.getContext().getString(R.string.by) + " " + repo.getOwner());
        view.setBackgroundResource(repo.isFork() ? R.color.white : R.color.light_green);

        checkIfNeedToLoadMoreItems(position);

        return view;
    }

    private void checkIfNeedToLoadMoreItems(int position) {
        int lastItemPosition = repos.size() - 1;
        if (loadMoreListener != null && position == lastItemPosition) {
            loadMoreListener.onLoadMore();
        }
    }

    private static class ViewHolder {
        TextView repoTextView;
        TextView ownerTextView;
        TextView descriptionTextView;
    }

    public void setLoadMoreListener(LoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

}
