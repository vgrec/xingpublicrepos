package com.vgrec.xingpublicrepos;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.vgrec.xingpublicrepos.adapter.LoadMoreListener;
import com.vgrec.xingpublicrepos.adapter.ReposAdapter;
import com.vgrec.xingpublicrepos.http.HttpRequestService;
import com.vgrec.xingpublicrepos.model.Repo;
import com.vgrec.xingpublicrepos.utils.ViewRepoDialog;

import java.util.ArrayList;
import java.util.List;


/**
 * Display the list of Xing repositories
 */
public class ReposListFragment extends Fragment {

    private static final int ITEMS_PER_PAGE = 10;
    private static final String DEFAULT_LINK = "https://api.github.com/users/xing/repos?per_page=" + ITEMS_PER_PAGE;
    private String nextLink;
    private String lastLink;

    /**
     * Used to identify if we made a request to the last repo page
     */
    private boolean lastLinkRequested;

    private ListView listView;
    private ProgressBar progressBar;
    private TextView errorTextView;

    private ArrayList<Repo> repos = new ArrayList<>();
    private ReposAdapter adapter;


    public ReposListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_repos, container, false);
        listView = (ListView) view.findViewById(R.id.repos_list_view);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        errorTextView = (TextView) view.findViewById(R.id.error_text);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = new ReposAdapter(getActivity(), repos);
        adapter.setLoadMoreListener(new LoadMoreListener() {
            @Override
            public void onLoadMore() {
                loadRepos();
            }
        });

        listView.setAdapter(adapter);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Repo repo = (Repo) parent.getItemAtPosition(position);
                showRepoUrlsDialog(repo);
                return true;
            }
        });

        // Load repos for the firs time
        loadRepos();
    }

    private void showRepoUrlsDialog(Repo repo) {
        ViewRepoDialog dialog = ViewRepoDialog.newInstance(new String[]{repo.getRepoUrl(), repo.getOwnerUrl()});
        dialog.setItemClickListener(new ViewRepoDialog.DialogItemClickListener() {
            @Override
            public void onItemClicked(String item) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(item));
                startActivity(browserIntent);
            }
        });
        dialog.show(getFragmentManager(), null);
    }

    private void loadRepos() {
        if (lastLinkRequested) {
            Toast.makeText(getActivity(), R.string.no_more_repos, Toast.LENGTH_LONG).show();
            return;
        }
        String url = (nextLink == null) ? DEFAULT_LINK : nextLink;

        // keep track that we issued a request with the last url,
        // so that the next requests to cancel
        if (url.equals(lastLink)) {
            lastLinkRequested = true;
        }
        Intent intent = HttpRequestService.makeIntent(getActivity(), url);
        getActivity().startService(intent);
        progressBar.setVisibility(View.VISIBLE);
    }

    private BroadcastReceiver downloadBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            progressBar.setVisibility(View.GONE);
            List<Repo> repos = intent.getParcelableArrayListExtra(HttpRequestService.RESULTS);
            nextLink = intent.getStringExtra(HttpRequestService.NEXT_LINK);
            lastLink = intent.getStringExtra(HttpRequestService.LAST_LINK);
            updateUi(repos);
        }
    };

    private void updateUi(List<Repo> repos) {
        if (repos == null) {
            errorTextView.setVisibility(View.VISIBLE);
            errorTextView.setText(R.string.could_not_retrieve_repos);
            return;
        }
        if (repos.size() <= 0) {
            errorTextView.setVisibility(View.VISIBLE);
            errorTextView.setText(R.string.no_repos);
            return;
        }

        this.repos.addAll(repos);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter(HttpRequestService.REPO_RESULTS);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(downloadBroadcastReceiver, intentFilter);
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(downloadBroadcastReceiver);
    }

}
