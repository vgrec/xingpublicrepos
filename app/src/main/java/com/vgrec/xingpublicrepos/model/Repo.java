package com.vgrec.xingpublicrepos.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * author Unknown, created on 02.06.2015.
 */
public class Repo implements Parcelable {

    private String name;
    private String description;
    private String owner;
    private boolean isFork;
    private String repoUrl;
    private String ownerUrl;

    public Repo(String name, String description, String owner, boolean isFork, String repoUrl, String ownerUrl) {
        this.name = name;
        this.description = description;
        this.owner = owner;
        this.isFork = isFork;
        this.repoUrl = repoUrl;
        this.ownerUrl = ownerUrl;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getOwner() {
        return owner;
    }

    public boolean isFork() {
        return isFork;
    }

    public String getRepoUrl() {
        return repoUrl;
    }

    public String getOwnerUrl() {
        return ownerUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(owner);
        dest.writeByte((byte) (isFork ? 1 : 0));
        dest.writeString(repoUrl);
        dest.writeString(ownerUrl);
    }

    private Repo(Parcel in) {
        name = in.readString();
        description = in.readString();
        owner = in.readString();
        isFork = in.readByte() != 0;
        repoUrl = in.readString();
        ownerUrl = in.readString();
    }

    public static final Parcelable.Creator<Repo> CREATOR = new Parcelable.Creator<Repo>() {
        public Repo createFromParcel(Parcel in) {
            return new Repo(in);
        }

        public Repo[] newArray(int size) {
            return new Repo[size];
        }
    };

}
